function onClick(){
    var f1 = parseInt(document.getElementById("field1").value);
    var f2 = parseInt(document.getElementById("field2").value);
    var re = /^\d+$/;
    if (re.test(f1) && re.test(f2)){    //только целые числа
        document.getElementById("answer").innerHTML = f1*f2;
    }
    else {
        alert("Сколько ж вас там?")
        document.getElementById("answer").innerHTML = "non";
    }
    return false;
}
window.addEventListener("DOMContentLoaded", function (event) {
    console.log("DOM fully loaded and parsed");
    let b = document.getElementById("button1");
    b.addEventListener("click", onClick);
    });